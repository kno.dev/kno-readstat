PKGNAME		  = readstat
LIBNAME		  = creadstat
KNOCONFIG         = knoconfig
KNOBUILD          = knobuild
INSTALL_SCOPE	  = shared
INSTALL_SCHEME    = install-scheme-source

prefix		 := $(shell $(KNOCONFIG) prefix)
libsuffix	 := $(shell $(KNOCONFIG) libsuffix)
CMODULES	 := $(DESTDIR)$(shell $(KNOCONFIG) cmodules $(INSTALL_SCOPE))
INSTALLMODS	 := $(DESTDIR)$(shell $(KNOCONFIG) modules $(INSTALL_SCOPE))
KNO_CFLAGS	 := -I. -fPIC $(shell $(KNOCONFIG) cflags)
KNO_LDFLAGS	 := -fPIC $(shell $(KNOCONFIG) ldflags)
KNO_LIBS	 := $(shell $(KNOCONFIG) libs)
LIB		 := $(shell $(KNOCONFIG) lib)
INCLUDE		 := $(shell $(KNOCONFIG) include)
KNO_VERSION	 := $(shell $(KNOCONFIG) version)
KNO_MAJOR	 := $(shell $(KNOCONFIG) major)
KNO_MINOR	 := $(shell $(KNOCONFIG) minor)
PKG_VERSION      := $(shell u8_gitversion ./etc/knomod_version)
PKG_MAJOR        := $(shell cat ./etc/knomod_version | cut -d. -f1)
FULL_VERSION     := ${KNO_MAJOR}.${KNO_MINOR}.${PKG_VERSION}
PATCHLEVEL       := $(shell u8_gitpatchcount ./etc/knomod_version)
PATCH_VERSION    := ${FULL_VERSION}-${PATCHLEVEL}
U8LIBINSTALL	 := $(shell which u8_install_shared 2>/dev/null || echo u8_install_shared)

INSTALLS	 := "$(shell pwd)/installs"

SUDO             := $(shell which sudo)
INIT_CFLAGS      := ${CFLAGS}
INIT_LDFAGS      := ${LDFLAGS}
DEBUG_CFLAGS	  = -g
CFLAGS	  	  = ${KNO_CFLAGS} ${DEBUG_CFLAGS} ${INIT_CFLAGS} ${XCFLAGS}
LDFLAGS	  	  = ${KNO_LDFLAGS} ${INIT_LDFLAGS} ${XCFLAGS}
LCFLAGS	  	  = -Iinstalls/include
LLDFLAGS  	  = -Linstalls/lib
LIBS		  = -lz -lbz2 -llzma $(NO_LIBS)

MKSO		  = $(CC) -shared $(LDFLAGS) $(LIBS)
SYSINSTALL        = /usr/bin/install -c
DIRINSTALL        = /usr/bin/install -d
MODINSTALL        = /usr/bin/install -m 0664
USEDIR        	  = /usr/bin/install -d
MSG		  = echo
MACLIBTOOL	  = $(CC) -dynamiclib -single_module -undefined dynamic_lookup \
			$(LDFLAGS)

GPGID            := ${OVERRIDE_GPGID:-FE1BC737F9F323D732AA26330620266BE5AFF294}
CODENAME	 := $(shell $(KNOCONFIG) codename)
REL_BRANCH	 := $(shell ${KNOBUILD} getbuildopt REL_BRANCH current)
REL_STATUS	 := $(shell ${KNOBUILD} getbuildopt REL_STATUS stable)
REL_PRIORITY	 := $(shell ${KNOBUILD} getbuildopt REL_PRIORITY medium)
ARCH             := $(shell ${KNOBUILD} getbuildopt BUILD_ARCH || uname -m)
APKREPO          := $(shell ${KNOBUILD} getbuildopt APKREPO /srv/repo/kno/apk)
APK_ARCH_DIR      = ${APKREPO}/staging/${ARCH}
RPMDIR		  = dist

STATICLIBS=installs/lib/libcsv.a installs/lib/libreadstat.a installs/lib/librdata.a

# Meta targets

# .buildmode contains the default build target (standard|debugging)
# debug/normal targets change the buildmode
# module build targets depend on .buildmode

default build: staticlibs .buildmode
	make $(shell cat .buildmode)

prep: ${STATICLIBS}

staticlibs: ${STATICLIBS}

module: staticlibs
	make creadstat.${libsuffix}

standard:
	make module
debugging:
	make XCFLAGS="-O0 -g3" module
production:
	make XCFLAGS="-O0 -g3" module

.buildmode:
	echo standard > .buildmode

debug:
	echo debugging > .buildmode
	make
normal:
	echo standard > .buildmode
	make
prod:
	echo production > .buildmode
	make

# Lower level rules

creadstat.o: creadstat.c makefile ${STATICLIBS} .buildmode
	@$(CC) --save-temps $(LCFLAGS) $(CFLAGS) -fPIC -D_FILEINFO="\"$(shell u8_fileinfo ./$< $(dirname $(pwd))/)\"" -o $@ -c $<
	@$(MSG) CC "(CREADSTAT)" $@
creadstat.so: creadstat.o makefile ${STATICLIBS}
	@$(MKSO) -o $@ creadstat.o -Wl,-soname=$(@F).${FULL_VERSION} \
	          -Wl,--allow-multiple-definition \
	          -Wl,--whole-archive ${STATICLIBS} -Wl,--no-whole-archive \
		 $(LLDFLAGS) $(LDFLAGS) $(LIBS)
	@$(MSG) MKSO "(CREADSTAT)" $@

creadstat.dylib: creadstat.o makefile ${STATICLIBS}
	$(MACLIBTOOL) -install_name \
		`basename $(@F) .dylib`.${KNO_MAJOR}.dylib \
		$(DYLIB_FLAGS) $(READSTAT_LDFLAGS) \
		-o $@ creadstat.o ${STATICLIBS}
	@$(MSG) MACLIBTOOL "(CREADSTAT)" $@

# Components

installs/lib/libcsv.a: libcsv/Makefile.am
	./build_libcsv $(INSTALLS)

installs/lib/libreadstat.a: installs/lib/libcsv.a
	./build_readstat $(INSTALLS)

installs/lib/librdata.a: installs/lib/libcsv.a
	./build_rdata $(INSTALLS)

libcsv/Makefile.am libreadstat/Makefile.am:
	git submodule update --init

staticlibs: ${STATICLIBS}
csvlib: installs/lib/libcsv.a
readstatlib rslib: installs/lib/libreadstat.a

readstat.dylib readstat.so: staticlibs

scheme/readstat.zip: scheme/readstat/*.scm
	cd scheme; zip readstat.zip readstat -x "*~" -x "#*" -x "*.attic/*" -x ".git*"

install: install-cmodule install-scheme
suinstall doinstall:
	sudo make install

${CMODULES}:
	@${DIRINSTALL} ${CMODULES}

install-cmodule: ${CMODULES}
	echo ${U8LIBINSTALL} ${LIBNAME}.${libsuffix} ${CMODULES} ${FULL_VERSION} "${SYSINSTALL}"
	${SUDO} ${U8LIBINSTALL} ${LIBNAME}.${libsuffix} ${CMODULES} ${FULL_VERSION} "${SYSINSTALL}"

${INSTALLMODS}/readstat:
	${SUDO} ${DIRINSTALL} $@

install-scheme: install-cmodule
	@make $(INSTALL_SCHEME)

install-scheme-source: ${INSTALLMODS}/readstat
	${SUDO} ${MODINSTALL} scheme/readstat/*.scm ${INSTALLMODS}/readstat

tests:
	cd testing; make
.PHONY: tests

clean:
	rm -f *.o *.so *.so.* *.dylib *.*.dylib
fresh: clean
	make

deepclean deep-clean: clean
	if [ -f libreadstat/Makefile ]; then cd libreadstat; make clean; fi
	if [ -f librdata/Makefile ]; then cd librdata; make clean; fi
	if [ -f libcsv/Makefile ]; then cd libcsv; make clean; fi
	rm -rf installs/bin installs/lib installs/include installs/share

deep-fresh: deep-clean
	make

gitup gitup-trunk:
	git checkout trunk && git pull

TAGS: creadstat.c scheme/readstat/*.scm
	etags -o $@ $^


