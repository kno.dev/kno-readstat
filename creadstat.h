/* -*- Mode: C;  Character-encoding: utf-8; -*- */

#ifndef KNO_CREADSTAT_H
#define KNO_CREADSTAT_H 1
#ifndef KNO_CREADSTAT_H_INFO
#define KNO_CREADSTAT_H_INFO "kno-postgres/creadstat.h"
#endif

typedef readstat_error_t (*readstat_parser_fn)(readstat_parser_t *,const char *,void *);

typedef struct KNO_READSTAT {
  KNO_ANNOTATED_HEADER;
  u8_string rs_source;
  u8_context rs_type;
  /* mtime for file used for metadata */
  time_t rs_filetime;
  /* Provided on open */
  lispval rs_options;
  lispval rs_fields;
  unsigned int rs_bits;
  unsigned char rs_ready; /* Schema/metadata are read */
  unsigned char rs_running; /* The parsefn is running */
  unsigned char rs_finished; /* All the data has been read */
  char rs_fullread; /* don't just read metadata */
  /* The parser object and the function we use with it, which is based on
     the file type (e.g. sas7bdat, stata por, etc) */
  long long rs_run_count;
  readstat_parser_t *rs_parser;
  readstat_parser_fn rs_parsefn;
  /* How text fields are encoded in the file */
  u8_encoding rs_text_encoding;
  /* This is used for looking up additional info for fields */
  /* This is the template used for generating data outputs */
  struct KNO_SCHEMAP *rs_template;
  /* When specified, this describes how columns in the data file map
     into the template. */
  /* Specifically sortmap[slot_i] = col_i means slot_i came from col_j */
  int *rs_sortmap;
  /* Or, inversely, inv_sortmap[col_i] = slot_i means col_i went to slot_j */
  int *rs_inv_sortmap;
  lispval *rs_template_info;
  lispval *rs_template_maps;
  lispval rs_framefn;
  /* What to do with generated observations */
  lispval rs_consumer;
  lispval rs_output;
  /* Extracted from the source metadata */
  long long rs_n_vars;
  long long rs_n_slots;
  long long rs_n_rows;
  long long rs_seen_vars;
  u8_string rs_table_name;
  u8_string rs_file_label;
  u8_string rs_enc_name;
  /* The offset of the ID field, if it exists */
  int rs_idpos;
  lispval rs_idslot;
  /* Other kinds of metadata */
  lispval rs_vlabels;
  lispval rs_colinfo;
  /* These track the current observation being read */
  long long rs_obsid;
  lispval *rs_values;
  struct KNO_SCHEMAP *rs_observation;

  u8_exception rs_exception;
  long int rs_err_count;
  long int rs_max_errs;

  /* Control the count/stop fields */
  u8_condvar rs_stopvar;
  u8_mutex rs_stoplock;
  long int rs_count;
  long int rs_row_offset;
  long int rs_row_limit;
  /* Pause in `init_observation` when rs_count >= rs_stop */
  long int rs_stop;} *kno_readstat;

/* Normalize case in column names */
#define KNO_READSTAT_FOLDCASE 0x100

/* Reorder slotids in result data frame (allows logN lookup) */
#define KNO_READSTAT_SORTMAP  0x200

#ifndef MAX_READSTAT_PARSERS
#define MAX_READSTAT_PARSERS 42
#endif

typedef struct CREADSTAT_PARSER {
  u8_string name;
  readstat_parser_fn fn; } *creadstat_parser;

#endif /* ndef KNO_CREADSTAT_H */


