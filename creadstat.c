/* -*- Mode: C; Character-encoding: utf-8; -*- */

/* readstat.c
   This implements Kno bindings to the ReadStat C library
   Copyright (C) 2021-2024 Kenneth Haase (ken.haase@alum.mit.edu)
*/

#ifndef _FILEINFO
#define _FILEINFO __FILE__
#endif

#define U8_INLINE_IO 1
#define KNO_DEFINE_GETOPT 1
#define KNO_SOURCE 1

#include "kno/knosource.h"
#include "kno/lisp.h"
#include "kno/numbers.h"
#include "kno/eval.h"
#include "kno/sequences.h"
#include "kno/storage.h"
#include "kno/texttools.h"
#include "kno/cprims.h"

#include "kno/sql.h"

#include <libu8/libu8.h>
#include <libu8/u8convert.h>
#include <libu8/u8pathfns.h>
#include <libu8/u8printf.h>
#include <libu8/u8crypto.h>
#include <libu8/u8filefns.h>

#if KNO_MAJOR_VERSION >= 2309
#define make_slotmap(space,len,inits) \
  (kno_init_slotmap(NULL,space,KNO_SLOTMAP_SORTED,len,inits))
#else
#define make_slotmap(space,len,inits) (kno_make_slotmap(space,len,inits))
#endif

u8_condition BadConsumer=_("BadReadstatConsumerName");

#include <math.h>
#include <limits.h>
#include <readstat.h>

static int utf8_warnings=1;

#define free_outstream(out) \
  if (out.u8_streaminfo&U8_STREAM_OWNS_BUF) u8_free(out.u8_outbuf);

/* Converting strings without an encoding */

static int get_utf8_size(u8_byte s1)
{
  if (s1 < 0x80) return 1;
  else if (s1 < 0xC0) return -1;
  else if (s1 < 0xE0) return 2;
  else if (s1 < 0xF0) return 3;
  else if (s1 < 0xF8) return 4;
  else if (s1 < 0xFC) return 5;
  else if (s1 < 0xFE) return 6;
  else return -1;
}

static int check_utf8_ptr(u8_string s,int size)
{
  int i=1;
  if (size<0)
    return 0;
  else if (size == 1)
    return size;
  /* Now check that the string is valid */
  while (i < size)
    if (s[i] < 0x80) return -i;
    else if (s[i] > 0xc0) return -i;
    else i++;
  return size;
}

static lispval makestring(u8_string in)
{
  int warned = 0;
  size_t len = strlen(in);
  if (len==0) return knostring(in);
  u8_string s = in;
  ssize_t init_len=len*2;
  if (init_len<16) init_len=16;
  U8_STATIC_OUTPUT(str,init_len);
  while (*s)
    if (*s<0x80) u8_putc(strout,*s++);
    else if (check_utf8_ptr(s,get_utf8_size(*s))>0) {
      int c=u8_sgetc(&s); u8_putc(strout,c);}
    else {
      if (warned==0) {
        U8_STATIC_OUTPUT(bug,len*2);
        u8_string scan=in; int byte=-1;
        while ((byte=*scan++)) {
          if (byte<0x80) u8_putc(bugout,byte);
          else u8_printf(bugout,"0x%x",byte);}
        u8_log(LOGERR,"BadUTF8","'%s'",u8_outstring(bugout));
        warned=1;}
      while (*s>=0x80) u8_putc(strout,*s++);}
  lispval result= kno_make_string(NULL,u8_outlen(strout),u8_outstring(strout));
  free_outstream(str);
  return result;
}

/*
   TODO:
   * Merge callback and output, include futures as output.
   * Add handling for missing and labelled values
   * Handle ctime/mtime metadata
   * Use encoding when provided (do we have to?)
   * Add measure and alignment to schema
 */

KNO_EXPORT int kno_initialize_creadstat(void) KNO_LIBINIT_FN;
static int readstat_initialized = 0;

kno_lisp_type kno_readstat_type;
#define KNO_READSTAT_TYPE     0x1C7900

static lispval choice_consumer, list_consumer, future_consumer;
static lispval countvec_consumer, basket_consumer;
static lispval templateid_symbol;

static lispval creadstat_module;

#include "creadstat.h"

static void readstat_recycler(struct KNO_READSTAT *rs);

static void reset_string(u8_string *ptr)
{
  if (*ptr) {
    u8_free(*ptr);
    *ptr=NULL;}
}

static struct CREADSTAT_PARSER readstat_parsers[MAX_READSTAT_PARSERS];
static int n_readstat_parsers = 0;
static u8_mutex readstat_parsers_lock;

static int kno_add_readstat_parser(u8_string name,readstat_parser_fn fn)
{
  u8_lock_mutex(&readstat_parsers_lock);
  int i = 0; while (i<n_readstat_parsers) {
    if (strcasecmp(name,readstat_parsers[i].name) == 0) {
      if (fn != readstat_parsers[i].fn)
        u8_log(LOGWARN,"ReadstatHandlerChanged",
               "Changing the readstat handler for '%s' from %p to %p",
               readstat_parsers[i].name,readstat_parsers[i].fn,fn);
      readstat_parsers[i].fn=fn;
      return i;}
    else i++;}
  if (i > MAX_READSTAT_PARSERS) {
    u8_seterr(_C("No more space for readstat parsers"),
              "kno_readstat_add_parser",NULL);
    u8_unlock_mutex(&readstat_parsers_lock);
    return -1;}
  int use_index = i;
  readstat_parsers[i].name = u8_strdup(name);
  readstat_parsers[i].fn = fn;
  n_readstat_parsers++;
  u8_unlock_mutex(&readstat_parsers_lock);
  return use_index;
}

static readstat_parser_fn lookup_readstat_parser(u8_string name)
{
  int i = 0; while (i<n_readstat_parsers) {
    if (strcasecmp(name,readstat_parsers[i].name) == 0) {
      return readstat_parsers[i].fn;}
    else i++;}
  return NULL;
}

static void finalize_readstat_template(kno_readstat rs);

/* Symbols used */

DEF_KNOSYM(nvars); DEF_KNOSYM(rows); DEF_KNOSYM(label); DEF_KNOSYM(tablename);
DEF_KNOSYM(encname); DEF_KNOSYM(is64bit); DEF_KNOSYM(idslot);
DEF_KNOSYM(formatversion); DEF_KNOSYM(name); DEF_KNOSYM(format);
DEF_KNOSYM(ctime); DEF_KNOSYM(mtime); DEF_KNOSYM(labels);
DEF_KNOSYM(labelset); DEF_KNOSYM(missing_ranges);
DEF_KNOSYM(measure); DEF_KNOSYM(nominal); DEF_KNOSYM(ordinal); DEF_KNOSYM(scale);
DEF_KNOSYM(compression); DEF_KNOSYM(binary); DEF_KNOSYM(framefn);
DEF_KNOSYM(alignment); DEF_KNOSYM(left); DEF_KNOSYM(right); DEF_KNOSYM(center);
DEF_KNOSYM(offset); DEF_KNOSYM(decimals);
DEF_KNOSYM(storage_width); DEF_KNOSYM(user_width); DEF_KNOSYM(display_width);
DEF_KNOSYM(start); DEF_KNOSYM(skip); DEF_KNOSYM(limit);
DEF_KNOSYM(string); DEF_KNOSYM(stringref);
DEF_KNOSYM(int8); DEF_KNOSYM(int16); DEF_KNOSYM(int32);
DEF_KNOSYM(float); DEF_KNOSYM(double);
DEF_KNOSYM(slotid); DEF_KNOSYM(consumer); DEF_KNOSYM(output);
DEF_KNOSYM(foldcase); DEF_KNOSYM(sortmap); DEF_KNOSYM(filetype);
DEF_KNOSYM(init); DEF_KNOSYM(run); DEF_KNOSYM(stop);
DEF_KNOSYM(val_labels); DEF_KNOSYM(maxerrs);
DEF_KNOSYM(fullread);

/* Constants */

static lispval system_missing_value;
static lispval tagged_missing_values[26];

/* Support for handlers */

static lispval stringval2lisp(const char *string,u8_encoding e)
{
  if (e) {
    struct U8_OUTPUT tmpout;
    ssize_t len = strlen(string);
    if (len == 0) return knostring(string);
    const unsigned char *instring = string;
    U8_INIT_STATIC_OUTPUT(tmpout,(len+len/4));
    u8_convert(e,1,&tmpout,&instring,instring+len);
    lispval result =
      kno_make_string(NULL,u8_outlen(&tmpout),u8_outstring(&tmpout));
    free_outstream(tmpout);
    return result;}
  else return makestring(string);
}

static lispval stringval2sym(const char *string,u8_encoding e)
{
  if (e) {
    struct U8_OUTPUT tmpout;
    ssize_t len = strlen(string);
    if (len == 0) return knostring(string);
    const unsigned char *instring = string;
    U8_INIT_STATIC_OUTPUT(tmpout,(len+len/4));
    u8_convert(e,1,&tmpout,&instring,instring+len);
    lispval result = kno_getsym(u8_outstring(&tmpout));
    free_outstream(tmpout);
    return result;}
  else if (*string)
    return kno_getsym(string);
  else return makestring(string);
}

static u8_string decode_string(const char *string,kno_readstat rs)
{
  if (string == NULL)
    return string;
  else if (rs->rs_text_encoding==NULL)
    return u8_strdup(string);
  else return u8_make_string
         (rs->rs_text_encoding,string,string+strlen(string));
}

static lispval decode_string2lisp(const char *string,kno_readstat rs,lispval nullval)
{
  if (string == NULL)
    return nullval;
  else return stringval2lisp(string,rs->rs_text_encoding);
}

static lispval decode_string2sym(const char *string,kno_readstat rs,lispval nullval)
{
  if (string == NULL)
    return nullval;
  else return stringval2sym(string,rs->rs_text_encoding);
}

static void store_string(lispval table,lispval slotid,u8_string value,
                         kno_readstat rs)
{
  lispval string = decode_string2lisp(value,rs,KNO_EMPTY);
  kno_store(table,slotid,string);
  kno_decref(string);
}

static void store_opt_string(lispval table,lispval slotid,u8_string value,
                             kno_readstat rs)
{
  lispval string = decode_string2lisp(value,rs,KNO_EMPTY);
  if (KNO_EMPTYP(string)) return;
  kno_store(table,slotid,string);
  kno_decref(string);
}

static u8_context get_readstat_typename(readstat_type_t type)
{
  switch (type) {
  case READSTAT_TYPE_STRING:
    return "READSTAT_TYPE_STRING";
  case READSTAT_TYPE_INT8:
    return "READSTAT_TYPE_INT8";
  case READSTAT_TYPE_INT16:
    return "READSTAT_TYPE_INT16";
  case READSTAT_TYPE_INT32:
    return "READSTAT_TYPE_INT32";
  case READSTAT_TYPE_FLOAT:
    return "READSTAT_TYPE_FLOAT";
  case READSTAT_TYPE_DOUBLE:
    return "READSTAT_TYPE_DOUBLE";
  case READSTAT_TYPE_STRING_REF:
    return "READSTAT_TYPE_STRING_REF";
  default:
    return "Bad type";
  }
}

static lispval get_readstat_typesym(readstat_type_t type)
{
  switch (type) {
  case READSTAT_TYPE_STRING:
    return KNOSYM(string);
  case READSTAT_TYPE_INT8:
    return KNOSYM(int8);
  case READSTAT_TYPE_INT16:
    return KNOSYM(int16);
  case READSTAT_TYPE_INT32:
    return KNOSYM(int32);
  case READSTAT_TYPE_FLOAT:
    return KNOSYM(float);
  case READSTAT_TYPE_DOUBLE:
    return KNOSYM(double);
  case READSTAT_TYPE_STRING_REF:
    return KNOSYM(stringref);
  default:
    return kno_intern("BadType");
  }
}

static u8_string get_valstring(readstat_value_t *val,kno_readstat rs)
{
  readstat_type_t valtype = val->type;
  switch (valtype) {
  case READSTAT_TYPE_STRING:
    return decode_string(val->v.string_value,rs);
  case READSTAT_TYPE_INT8:
    return u8_mkstring("%d",val->v.i8_value);
  case READSTAT_TYPE_INT16:
    return u8_mkstring("%d",val->v.i16_value);
  case READSTAT_TYPE_INT32:
    return u8_mkstring("%d",val->v.i32_value);
  case READSTAT_TYPE_FLOAT:
    return u8_mkstring("%f",val->v.float_value);
  case READSTAT_TYPE_DOUBLE:
    return u8_mkstring("%f",val->v.double_value);
  case READSTAT_TYPE_STRING_REF:
    return u8_strdup(val->v.string_value);
  default:
    return "Bad type";
  }
}

static lispval get_lisp_value(readstat_value_t *val,kno_readstat rs)
{
  if (val->is_system_missing)
    return system_missing_value;
  else if ( (val->is_tagged_missing) && (val->tag>='a') && (val->tag<='z'))
    return tagged_missing_values[val->tag-'a'];
  else NO_ELSE; /* just continue */
  readstat_type_t valtype = val->type;
  switch (valtype) {
  case READSTAT_TYPE_STRING: {
    if (val->v.string_value==NULL)
      return KNO_FALSE;
    else if (rs->rs_text_encoding)
      return stringval2lisp(val->v.string_value,rs->rs_text_encoding);
    else return makestring(val->v.string_value);}
  case READSTAT_TYPE_INT8:
    return KNO_INT(val->v.i8_value);
  case READSTAT_TYPE_INT16:
    return KNO_INT(val->v.i16_value);
  case READSTAT_TYPE_INT32:
    return KNO_INT(val->v.i32_value);
  case READSTAT_TYPE_FLOAT:
    return kno_make_flonum(val->v.float_value);
  case READSTAT_TYPE_DOUBLE:
    return kno_make_flonum(val->v.double_value);
  case READSTAT_TYPE_STRING_REF: {
    if (rs->rs_text_encoding)
      return stringval2lisp(val->v.string_value,rs->rs_text_encoding);
    else return makestring(val->v.string_value);}
  default:
    return KNO_VOID;
  }
}

static void add_value_label(kno_readstat rs,
			    const char *labelset,
			    const char *label,
			    lispval v)
{
  lispval labels = rs->rs_vlabels;
  if (!(KNO_CONSP(labels)))
    labels=rs->rs_vlabels=make_slotmap(3,0,NULL);
  lispval label_set_name = decode_string2sym(labelset,rs,KNO_FALSE);
  lispval label_set = kno_get(labels,label_set_name,KNO_VOID);
  if (KNO_VOIDP(label_set)) {
    label_set=make_slotmap(16,0,NULL);
    kno_store(labels,label_set_name,label_set);}
  lispval label_string = decode_string2lisp(label,rs,KNO_EMPTY);
  kno_store(label_set,label_string,v);
  kno_decref(label_string);
  kno_decref(label_set_name);
  kno_decref(label_set);
}

/* Readstat handler functions */

#define TEMPLATE_FLAGS (KNO_SCHEMAP_PRIVATE|KNO_SCHEMAP_DATAFRAME)

static int metadata_handler(readstat_metadata_t *md,void *state)
{
  struct KNO_READSTAT *rs = (kno_readstat) state;
  /* Supposedly have all the metadata */
  if (rs->rs_ready) return READSTAT_HANDLER_OK;

  int n_vars = md->var_count, n_slots = n_vars;
  lispval annotations = rs->annotations;
  /* Normalize idslot to NULL */
  if ((KNO_VOIDP(rs->rs_idslot))||(KNO_FALSEP(rs->rs_idslot)))
    rs->rs_idslot=KNO_NULL;
  else {
    rs->rs_idpos=n_slots;
    n_slots++;}
  lispval *schema = u8_alloc_n(n_slots,lispval);
  int i = 0; while (i<n_slots) {schema[i]=KNO_INT2FIX(i); i++;}
  struct KNO_SCHEMAP *template;
  lispval dfptr = kno_make_schemap(NULL,n_slots,TEMPLATE_FLAGS,schema,NULL);
  if (KNO_ABORTED(dfptr)) {
    u8_free(schema);
    return dfptr;}
  else template = (kno_schemap) dfptr;
  lispval id = kno_getopt(rs->rs_options,templateid_symbol,KNO_VOID);
  if (!(KNO_VOIDP(id))) template->schemap_source = id;
  if (rs->rs_template) {
    lispval v = (lispval) rs->rs_template;
    kno_decref(v);}
  rs->rs_template = template;
  if (rs->rs_colinfo) kno_decref(rs->rs_colinfo);
  rs->rs_colinfo = (n_slots>16) ?
    (kno_make_hashtable(NULL,n_slots*2)) :
    (make_slotmap(n_slots,0,NULL));
  rs->rs_n_vars    = n_vars;
  rs->rs_n_slots   = n_slots;
  rs->rs_n_rows    = md->row_count;
  rs->rs_seen_vars = 0;
  reset_string(&(rs->rs_table_name));
  rs->rs_table_name = decode_string(md->table_name,rs);
  reset_string(&(rs->rs_file_label));
  rs->rs_file_label = decode_string(md->file_label,rs);
  reset_string(&(rs->rs_enc_name));
  rs->rs_enc_name = decode_string(md->file_encoding,rs);
  if (md->creation_time>0)  {
    lispval timestamp = kno_time2timestamp(md->creation_time);
    kno_store(annotations,KNOSYM(ctime),timestamp);
    kno_decref(timestamp);}
  if (md->modified_time>0)  {
    lispval timestamp = kno_time2timestamp(md->modified_time);
    kno_store(annotations,KNOSYM(mtime),timestamp);
    kno_decref(timestamp);}
  if (rs->rs_idslot) {
    /* We always store the idslot in the last (un-normalized) slot */
    lispval *schema = template->table_schema;
    lispval *values = template->table_values;
    struct KNO_KEYVAL keyvals[2] = {
      { KNOSYM_SLOTID, rs->rs_idslot },
      { KNOSYM_TYPE, KNO_FIXNUM_TYPE } };
    schema[rs->rs_n_vars]=rs->rs_idslot;
    values[rs->rs_n_vars]=make_slotmap(4,2,keyvals);
    kno_store(annotations,KNOSYM(idslot),rs->rs_idslot);}
  if (md->var_count>=0) {
    kno_store(annotations,KNOSYM(nvars),KNO_INT(md->var_count));}
  if (md->row_count>=0) {
    kno_store(annotations,KNOSYM(rows),KNO_INT(md->row_count));}
  if (md->table_name)
    store_string(annotations,KNOSYM(tablename),md->table_name,rs);
  if (md->file_label)
    store_opt_string(annotations,KNOSYM(label),md->file_label,rs);
  if (md->file_encoding) {
    store_opt_string(annotations,KNOSYM(encname),md->file_encoding,rs);
    rs->rs_text_encoding=u8_get_encoding(md->file_encoding);}
  switch (md->compression) {
  case READSTAT_COMPRESS_ROWS:
    kno_store(annotations,KNOSYM(compression),KNOSYM(rows));
    break;
  case READSTAT_COMPRESS_BINARY:
    kno_store(annotations,KNOSYM(compression),KNOSYM(binary));
    break;
  case READSTAT_COMPRESS_NONE:
    break;
  }
  kno_store(annotations,KNOSYM(is64bit),(md->is64bit)?(KNO_TRUE):(KNO_FALSE));
  if (md->file_format_version>0)
    kno_store(annotations,KNOSYM(formatversion),KNO_INT(md->file_format_version));
 finished:
  return READSTAT_HANDLER_OK;
}

#define COPY_INT_PROP(vd,field)						\
  if (vd->field>0) kno_store(slot_info,KNOSYM(field),KNO_INT(vd->field))

static int variable_handler(int ix,readstat_variable_t *vd,
                            const char *value_labels,
                            void *state)
{
  struct KNO_READSTAT *rs = (kno_readstat) state;
  /* Supposedly have all the metadata */
  if (rs->rs_ready) return READSTAT_HANDLER_OK;

  struct KNO_SCHEMAP *template = rs->rs_template;
  lispval *schema = template->table_schema;
  lispval *values = template->table_values;
  lispval fieldinfo = rs->rs_fields;
  lispval colinfo = rs->rs_colinfo;
  int n = template->schema_length, i = vd->index;
  if (i>n) {
    u8_byte details_buf[200];
    kno_seterr(_C("ReadStatError"),"variable_handler",
	       u8_bprintf(details_buf,"Index %d for %s is too big (> %d)",
			  i,vd->name,n),
	       KNO_VOID);
    return -1;}
  u8_string field_name = vd->name;
  struct KNO_STRING _tempstring;
  lispval tempstring = kno_init_string(&_tempstring,-1,field_name);
  lispval known = kno_getopt(fieldinfo,tempstring,KNO_VOID);
  lispval slotid = ( (KNO_SYMBOLP(known)) || (KNO_OIDP(known)) ) ? (known) :
    (KNO_TABLEP(known)) ? (kno_getopt(known,KNOSYM_SLOTID,KNO_VOID)) :
    (KNO_VOID);
  if (!((KNO_SYMBOLP(slotid))||(KNO_OIDP(slotid)))) {
    kno_decref(slotid);
    slotid = (((rs->rs_bits)&(KNO_READSTAT_FOLDCASE)) ?
              (kno_getsym(field_name)) : (kno_intern(field_name)));}
  lispval info = (TABLEP(known)) ? (known) : (kno_getopt(fieldinfo,slotid,KNO_FALSE));
  lispval slot_info = make_slotmap(17,0,NULL);
  schema[i]=slotid;
  if ( (KNO_CONSP(info)) && (KNO_OPTIONSP(info)) )
    values[i]=kno_init_pair(NULL,slot_info,info);
  else values[i]=slot_info;
  kno_store(slot_info,KNOSYM(slotid),slotid);
  store_string(slot_info,KNOSYM(name),vd->name,rs);
  store_string(slot_info,KNOSYM(format),vd->format,rs);
  store_string(slot_info,KNOSYM(label),vd->label,rs);
  COPY_INT_PROP(vd,offset);
  COPY_INT_PROP(vd,storage_width);
  COPY_INT_PROP(vd,user_width);
  COPY_INT_PROP(vd,display_width);
  COPY_INT_PROP(vd,decimals);
  COPY_INT_PROP(vd,skip);
  if (vd->label_set) {
    u8_string label_set_name = vd->label_set->name;
    store_string(slot_info,KNOSYM(labelset),label_set_name,rs);}
  if (vd->missingness.missing_ranges_count) {
    int n = vd->missingness.missing_ranges_count;
    lispval vec = kno_make_vector(n,NULL);
    int i = 0; while (i<n) {
      lispval v = get_lisp_value(&(vd->missingness.missing_ranges[i]),rs);
      KNO_VECTOR_SET(vec,i,v);
      i++;}
    kno_store(slot_info,KNOSYM(missing_ranges),vec);
    kno_decref(vec);}
  switch (vd->measure) {
  case READSTAT_MEASURE_NOMINAL:
    kno_store(slot_info,KNOSYM(measure),KNOSYM(nominal));
    break;
  case READSTAT_MEASURE_ORDINAL:
    kno_store(slot_info,KNOSYM(measure),KNOSYM(ordinal));
    break;
  case READSTAT_MEASURE_SCALE:
    kno_store(slot_info,KNOSYM(measure),KNOSYM(scale));
    break;
  case READSTAT_MEASURE_UNKNOWN:
    break;
  }
  switch (vd->alignment) {
  case READSTAT_ALIGNMENT_LEFT:
    kno_store(slot_info,KNOSYM(alignment),KNOSYM(left));
    break;
  case READSTAT_ALIGNMENT_RIGHT:
    kno_store(slot_info,KNOSYM(alignment),KNOSYM(right));
    break;
  case READSTAT_ALIGNMENT_CENTER:
    kno_store(slot_info,KNOSYM(alignment),KNOSYM(center));
    break;
  case READSTAT_ALIGNMENT_UNKNOWN:
    break;
  }
  kno_store(slot_info,KNOSYM_TYPE,get_readstat_typesym(vd->type));
  if (value_labels) {
    store_string(slot_info,KNOSYM(val_labels),value_labels,rs);}
  { lispval field_name = stringval2sym(vd->name,rs->rs_text_encoding);
    kno_store(colinfo,field_name,slot_info);
    kno_decref(field_name);}

  rs->rs_seen_vars++;

  if ( rs->rs_seen_vars == rs->rs_n_vars ) {
    if (!(rs->rs_ready))
      finalize_readstat_template(rs);}

  return READSTAT_HANDLER_OK;
}

static int label_handler(const char *labelset,
			 readstat_value_t value,
			 const char *label,
			 void *state)
{
  struct KNO_READSTAT *rs = (kno_readstat) state;
  lispval v = get_lisp_value(&value,rs);
  u8_log(LOGWARN,"Label","%s:%s=%q",labelset,label,v);
  add_value_label(rs,labelset,label,v);
  kno_decref(v);
  return READSTAT_HANDLER_OK;
}

static int progress_handler(double progress, void *state)
{
  struct KNO_READSTAT *rs = (kno_readstat) state;
  if ( ! (rs->rs_ready) )
    return READSTAT_HANDLER_OK;
  if (rs->rs_fullread == 0)
    return READSTAT_HANDLER_ABORT;
  else return READSTAT_HANDLER_OK;
}

static int redundant_sortmapp(unsigned int *map,int n)
{
  int i = 0; while (i<n) {
    if ( map[i] == i ) i++;
    else if ( map[i] > n ) return -1;
    else return 0;}
  return 1;
}

DEF_KNOSYM(valmap); DEF_KNOSYM(colnames); DEF_KNOSYM(colids);

static void finalize_readstat_template(kno_readstat rs)
{
  if (rs->rs_ready) return;
  struct KNO_SCHEMAP *template = rs->rs_template;
  unsigned int n_slots = template->schema_length;
  lispval *schema = template->table_schema;
  lispval *slotinfo = template->table_values;
  lispval slotvec=kno_make_vector(n_slots,schema);
  kno_store(rs->annotations,KNOSYM(colids),slotvec);
  kno_decref(slotvec);
  {
    lispval colnames = kno_make_vector(n_slots,NULL);
    lispval *vecdata = KNO_VECTOR_ELTS(colnames);
    int i = 0; while (i<n_slots) {
      lispval name = kno_get(slotinfo[i],KNOSYM(name),KNO_FALSE);
      vecdata[i++]=name;}
    kno_store(rs->annotations,KNOSYM(colnames),colnames);
    kno_decref(colnames);}
  if ((rs->rs_bits)&(KNO_READSTAT_SORTMAP)) {
    /* We temporarily copy the template values to a separate vector
       so we don't have to worry about conflicting slot swaps */
    lispval *native = u8_alloc_n(n_slots,lispval);
    kno_lspcpy(native,slotinfo,n_slots);
    /* Normalize (sort by pointer value) the schema, and get the
       resulting slot swaps */
    int *inv_sortmap = NULL;
    int *sortmap = kno_norm_schema(n_slots,schema,&inv_sortmap,NULL);
    int redundant = redundant_sortmapp(sortmap,n_slots);
    if (redundant>0) {
      KNO_XTABLE_SET_BIT(template,KNO_SCHEMAP_SORTED,1);
      if (inv_sortmap) u8_free(inv_sortmap);
      u8_free(sortmap);
      u8_free(native);}
    else if (redundant<0) {
      u8_log(LOGERR,"ReadstatError",
             "Normalization failed for schema %q for %s",
             (lispval)rs,rs->rs_source);
      if (inv_sortmap) u8_free(inv_sortmap);
      u8_free(sortmap);
      u8_free(native);}
    else {
      /* Shuffle the slotinfo values */
      int i = 0; while (i<n_slots) {
        slotinfo[i]=native[inv_sortmap[i]];
        i++;}
      rs->rs_sortmap = sortmap;
      rs->rs_inv_sortmap = inv_sortmap;
      KNO_XTABLE_SET_BIT(template,KNO_SCHEMAP_SORTED,1);
      if (rs->rs_idpos>=0) {
        rs->rs_idpos=inv_sortmap[rs->rs_idpos];}
      u8_free(native);}}
  rs->rs_template_info = slotinfo;
  lispval *colmaps = NULL;
  int i = 0; while (i<n_slots) {
    lispval info = slotinfo[i], valmap = KNO_VOID;
    if ( (KNO_CONSP(info)) && (KNO_OPTIONSP(info)) &&
         (kno_testopt(info,KNOSYM(valmap),KNO_VOID)) ) {
      lispval check = kno_getopt(info,KNOSYM(valmap),KNO_VOID);
      if (KNO_ISVALMAP(info))
        valmap=check;
      else kno_decref(check);}
    else if (KNO_ISVALMAP(info))
      valmap=kno_incref(info);
    else NO_ELSE;
    if (!(KNO_VOIDP(valmap))) {
      if (colmaps == NULL) {
        colmaps = u8_alloc_n(n_slots,lispval);
        kno_lspset(colmaps,KNO_FALSE,n_slots);}
      colmaps[i]=valmap;}
    i++;}
  rs->rs_template_maps=colmaps;
  rs->rs_ready = 1;
}

/* Handling actual data */

static void add_to_countvec(lispval countvec,lispval observation);

static int apply_callback(kno_readstat rs,int obsid,lispval observation,
                          lispval consumer,int n_args,kno_argvec args)
{
  lispval result = kno_apply(consumer,n_args,args);
  if (KNO_TROUBLEP(result)) {
    u8_exception ex = u8_erreify();
    if (ex)
      u8_log(LOGERR,"ReadStatCallbackError",
             "%s<%s>(%s) applying %q to observation %lld= %q",
             ex->u8x_cond,ex->u8x_context,ex->u8x_details,
             consumer,obsid,observation);
    else u8_log(LOGERR,"ReadStatCallbackError",
                "Unknown error applying %q to observation %lld= %q",
                consumer,obsid,observation);
    rs->rs_err_count++;
    if ( (rs->rs_max_errs<0) || (rs->rs_err_count<rs->rs_max_errs) ) {
      u8_free_exception(ex,0);}
    else {
      rs->rs_exception=ex;
      kno_decref(observation);
      return -1;}}
  else kno_decref(result);
  kno_decref(observation);
  return 1;
}

static int consume_observation(kno_readstat rs,lispval observation,int obsid)
{
  lispval consumer=rs->rs_consumer, output=rs->rs_output;
  if (KNO_FALSEP(consumer)) {
    kno_decref(observation);
    return 0;}
  else if (KNO_SYMBOLP(consumer)) {
    if (obsid<0) {/* Nothing special at end of data */}
    else if ( consumer == list_consumer)
      rs->rs_output = kno_init_pair(NULL,observation,rs->rs_output);
    else if ( consumer == choice_consumer) {
      kno_prechoice_add((kno_prechoice)(rs->rs_output),observation);}
    else if ( consumer == future_consumer ) {
      kno_change_future((kno_future)(rs->rs_output),observation,
                        KNO_FUTURE_ONESHOT);
      kno_decref(observation);}
    else if ( consumer == basket_consumer ) {
      kno_prechoice_add((kno_prechoice)(rs->rs_output),observation);}
    else if ( consumer == countvec_consumer ) {
      add_to_countvec(observation,rs->rs_output);}
    else {
      kno_seterr(BadConsumer,"consume_observation",KNO_SYMBOL_NAME(consumer),
                 observation);
      kno_decref(observation);
      return -1;}
    return 1;}
  else if (KNO_APPLICABLEP(consumer)) {
    lispval proc = consumer;
    int i = 0, n_args = 5;
    if (KNO_PROCP(consumer)) {
      struct KNO_PROC *info=KNO_PROC_INFO(consumer);
      if (info->proc_arity<0) {}
      else if (info->proc_min_arity<n_args)
        n_args=info->proc_min_arity;
      else NO_ELSE;}
    lispval args[5] = { KNO_VOID };
    if (i<n_args) args[i++]=observation;
    if (i<n_args) args[i++]=KNO_INT(obsid);
    if (i<n_args) args[i++]=((lispval)rs);
    if (i<n_args) args[i++]=rs->rs_output;
    return apply_callback(rs,obsid,observation,consumer,n_args,args);}
  else if ( (KNO_PAIRP(consumer)) && (KNO_APPLICABLEP(CAR(consumer))) ) {
    lispval proc = KNO_CAR(consumer), arginfo = KNO_CDR(consumer);
    int arity = (KNO_PROCP(consumer)) ? (((kno_proc)consumer)->proc_arity) : (-1);
    int extra_args = KNO_VECTORP(arginfo) ? (KNO_VECTOR_LENGTH(arginfo)) : (1);
    int i = 0, max_args = 4+extra_args;
    int n_args = ( (arity<0) || (arity>max_args) ) ? (max_args) : (arity);
    int item_args = n_args-extra_args;
    lispval args[max_args];
    if (! (VECTORP(arginfo)) )
      args[i++] = output;
    else NO_ELSE;
    if (item_args>0) args[i++]=observation;
    if (item_args>1) args[i++]=KNO_INT(obsid);
    if (item_args>2) args[i++]=((lispval)rs);
    if (item_args>3) args[i++]=rs->rs_output;
    if (VECTORP(arginfo)) {
      kno_lspcpy(args+i,KNO_VECTOR_ELTS(arginfo),extra_args);}
    return apply_callback(rs,obsid,observation,consumer,n_args,args);}
  else {
    u8_byte buf[32];
    kno_seterr(BadConsumer,"consume_observation",
               u8_bprintf(buf,"obs#%d",obsid),
               observation);
    kno_decref(observation);
    return -1;}
  return 1;
}

#define SAMPLE_FLAGS  (KNO_DATAFRAME_SCHEMAP|KNO_SCHEMAP_STATIC_VALUES)

static int finish_observation(kno_readstat rs);

static int init_observation(kno_readstat rs,long long obsv)
{
  if (! rs->rs_ready) finalize_readstat_template(rs);
  if ( (rs->rs_stop>=0) && (rs->rs_count>=rs->rs_stop) ) {
    while ( (rs->rs_stop>=0) && (rs->rs_count>=rs->rs_stop) ) {
      u8_condvar_wait(&(rs->rs_stopvar),&(rs->rs_stoplock));}}
  if (rs->rs_obsid == obsv) return 0;
  else if (obsv<0) return 0;
  else if (rs->rs_obsid >=0 ) {
    int rv=finish_observation(rs);
    if (rv<0) return rv;}
  else NO_ELSE;
  rs->rs_obsid = obsv;
  struct KNO_SCHEMAP *df = rs->rs_template;
  int n = df->schema_length;
  lispval sv = kno_cons_sample(SAMPLE_FLAGS,df,n,NULL);
  struct KNO_SCHEMAP *observation = (kno_schemap) sv;
  lispval *values = observation->table_values;
  rs->rs_values = values;
  rs->rs_observation = observation;
  if (rs->rs_idpos>=0)
    values[rs->rs_idpos] = KNO_INT(obsv);
  return rs->rs_count++;
}

static int value_handler(int obs_index,
			 readstat_variable_t *vd,
			 readstat_value_t val,
			 void *state)
{
  int rv = 0;
  struct KNO_READSTAT *rs = (kno_readstat) state;
  /* If we have a new observation id, finish the current observation
     and start a new one */
  if (obs_index != rs->rs_obsid) {
    if (rs->rs_obsid>=0) {
      rv=finish_observation(rs);
      if (rv<0) return READSTAT_HANDLER_ABORT;}
    rv=init_observation(rs,obs_index);
    if (rv<0) return READSTAT_HANDLER_ABORT;}
  int var_index = vd->index;
  lispval *values = rs->rs_values;
  int *sortmap = rs->rs_sortmap;
  lispval v = get_lisp_value(&val,rs);
  int out_index = (sortmap) ? (sortmap[var_index]) : (var_index);
  if (out_index>=0)
    values[out_index]=v;
  return READSTAT_HANDLER_OK;
}

static int finish_observation(kno_readstat rs)
{
  if ( (rs->rs_obsid >= 0) && (rs->rs_observation) ) {
    lispval observation = (lispval) rs->rs_observation;
    lispval *values = rs->rs_values;
    rs->rs_observation = NULL;
    rs->rs_values = NULL;
    int obsid = rs->rs_obsid, rv=-1; rs->rs_obsid = -1;
    if (rs->rs_template_maps) {
      int n_slots = rs->rs_template->schema_length;
      kno_lispvec_dotmap(n_slots,rs->rs_template_maps,values);}
    if ( (KNO_FALSEP(rs->rs_framefn)) || (KNO_VOIDP(rs->rs_framefn)) )
      rv=consume_observation(rs,observation,obsid);
    else {
      lispval final = kno_apply_outputfn(rs->rs_framefn,observation);
      if (ABORTED(final))
        rv=-1;
      else if (! ( (KNO_VOIDP(final)) || (KNO_EMPTYP(final)) ) )
        rv=consume_observation(rs,final,obsid);
      else rv=consume_observation(rs,observation,obsid);}
    return rv;}
  else return 0;
}

/* Countvecs for accumulating results */

static void add_to_countvec(lispval countvec,lispval observation)
{
  if (USUALLY ( (PAIRP(countvec)) &&
                (KNO_FIXNUMP(KNO_CAR(countvec))) &&
                (KNO_VECTORP(KNO_CDR(countvec))) ) ) {
    ssize_t n_elts = KNO_FIX2INT(KNO_CAR(countvec));
    lispval vec = KNO_CDR(countvec);
    ssize_t veclen = KNO_VECTOR_LENGTH(vec);
    if (n_elts>=veclen) {
      ssize_t delta = (veclen<100) ? (5) :
        (veclen<1000) ? (100) :
        (1000);
      ssize_t new_len = veclen+delta;
      lispval new_vec = kno_make_vector(new_len,NULL);
      kno_lspcpy(KNO_VECTOR_ELTS(new_vec),KNO_VECTOR_ELTS(vec),veclen);
      kno_lspset(KNO_VECTOR_ELTS(new_vec)+veclen,KNO_FALSE,new_len-veclen);
      lispval old_vec=vec;
      kno_vector free_vec = (kno_vector) vec;
      vec=new_vec;
      veclen=new_len;
      free_vec->vec_free_elts=0;
      kno_decref(vec);}
    KNO_VECTOR_SET(vec,n_elts,observation);
    lispval fixcount = KNO_INT2FIX(n_elts+1);
    KNO_SETCDR(countvec,fixcount);}
  else {
    /* TODO: log bad countvec? */
    kno_decref(observation);}
}

static int grab_readstat_err(readstat_error_t err,u8_context cxt,lispval rs) {
  if (err != READSTAT_OK) {
    kno_seterr(_C("ReadStatError"),cxt,readstat_error_message(err),rs);
    kno_decref(rs);
    return -1;}
  else return 0;
}

#define CHECK_READSTAT_ERR(err,cxt,rs,retval)                   \
  if (err != READSTAT_OK) {                                     \
    grab_readstat_err(err,cxt,(lispval)rs); return retval; }    \
  else NO_ELSE;

/* Making readstats */

static kno_readstat new_readstat
(u8_string source,u8_string filetype,lispval opts,
 lispval consumer,lispval output)
{
  u8_string type_name = NULL;
  if ( (source == NULL) || (!(u8_file_existsp(source))) ) {
    kno_seterr(_C("NoSuchFile"),"new_readstat",source,opts);
    return NULL;}
  else source=u8_abspath(source,NULL);
  if (filetype == NULL) {
    lispval from_opts = kno_getopt(opts,KNOSYM(filetype),KNO_VOID);
    if (KNO_VOIDP(from_opts)) {}
    else if (KNO_STRINGP(from_opts))
      type_name = u8_strdup(KNO_CSTRING(from_opts));
    else if (KNO_SYMBOLP(from_opts))
      type_name = u8_strdup(KNO_SYMBOL_NAME(from_opts));
    else {}
    kno_decref(from_opts);}
  else type_name = u8_strdup(filetype);
  if (type_name == NULL) {
    kno_seterr(_C("NoFileType"),"new_readstat",NULL,opts);
    return NULL;}
  readstat_parser_fn parsefn = lookup_readstat_parser(type_name);
  if (parsefn == NULL) {
    kno_seterr(_C("UnknownFileType"),"new_readstat",type_name,opts);
    u8_free(type_name);
    return NULL;}

  struct KNO_READSTAT *result=u8_alloc(struct KNO_READSTAT);
  KNO_INIT_CONS(result,kno_readstat_type);
  lispval annotations = result->annotations  = make_slotmap(17,0,NULL);
  kno_store(annotations,KNOSYM_OPTS,opts);

  result->rs_source=source;
  result->rs_type=type_name;
  result->rs_filetime=-1;
  result->rs_parsefn=parsefn;
  result->rs_parser=NULL;
  result->rs_bits = 0;
  if (kno_testopt(opts,KNOSYM(fullread),KNO_VOID))
    result->rs_fullread=-1;
  else result->rs_fullread = 0;

  result->rs_n_vars = -1;
  result->rs_ready = 0;
  result->rs_running = 0;
  result->rs_finished = 0;
  result->rs_text_encoding = NULL;
  result->rs_idslot = kno_getopt(opts,KNOSYM(idslot),KNO_VOID);
  result->rs_vlabels = kno_getopt(opts,KNOSYM(labels),KNO_EMPTY);

  result->rs_colinfo         = KNO_VOID;
  result->rs_template        = NULL;
  result->rs_sortmap         = NULL;
  result->rs_template_info   = NULL;
  result->rs_template_maps   = NULL;

  result->rs_options = kno_incref(opts);
  result->rs_fields  = kno_getopt(opts,KNOSYM_FIELDS,KNO_FALSE);

  result->rs_obsid     = -1;
  result->rs_observation = NULL;

  /* Setting up the handling of output dataframes */

  if (KNO_VOIDP(consumer))
    consumer = kno_getopt(opts,KNOSYM(consumer),KNO_VOID);
  else kno_incref(consumer);
  if (KNO_VOIDP(output))
    output = kno_getopt(opts,KNOSYM(output),KNO_VOID);
  else kno_incref(output);
  if (KNO_APPLICABLEP(consumer)) {
    if (KNO_PROCP(consumer)) {
      struct KNO_PROC *info = KNO_PROC_INFO(consumer);
      if ( (info->proc_arity==0) || (info->proc_min_arity>5) ){
        u8_condition c = (info->proc_arity==0) ? (kno_TooManyArgs) : (kno_TooFewArgs);
        kno_seterr(c,"readstat_consumer",NULL,consumer);
        kno_decref(output); kno_decref(consumer);
        goto err_exit;}
      else NO_ELSE;}
    result->rs_consumer = consumer;
    result->rs_output = output;}
  else if ( (KNO_PAIRP(consumer)) && (KNO_APPLICABLEP(KNO_CAR(consumer))) ) {
    lispval car = KNO_CAR(consumer), cdr = KNO_CDR(consumer);
    struct KNO_PROC *info = KNO_PROC_INFO(car);
    int extra_args = 1;
    if (KNO_VECTORP(cdr))
      extra_args=KNO_VECTOR_LENGTH(cdr);
    else if (KNO_PAIRP(cdr)) {
      int n = kno_list_length(cdr);
      lispval vec_args = kno_make_vector(n,NULL);
      int i = 0; lispval scan = cdr;
      while (PAIRP(scan)) {
        KNO_VECTOR_SET(vec_args,i,CAR(scan));
        scan=CDR(scan);
        i++;}
      kno_incref(car);
      lispval new_consumer=kno_init_pair(NULL,car,vec_args);
      kno_decref(consumer);
      consumer=new_consumer;
      extra_args=n;}
    else {
      kno_seterr("BadConsumer","new_readatat",source,consumer);
      goto err_exit;}
    if (info) {
      int min_args = extra_args+1, max_args = extra_args+5;
      if ( ( (info->proc_arity>0) && (info->proc_arity<min_args) ) ||
           (info->proc_min_arity>max_args) ) {
        u8_condition c = ( (info->proc_arity>0) && (info->proc_arity<min_args) ) ?
          (kno_TooManyArgs) :
          (kno_TooFewArgs);
        kno_seterr(c,"readstat_consumer",NULL,consumer);
        kno_decref(output); kno_decref(consumer);
        goto err_exit;}
      else NO_ELSE;}
    result->rs_consumer = kno_incref(consumer);
    result->rs_output   = output;}
  else if (KNO_TYPEP(consumer,kno_future_type)) {
    result->rs_output = consumer;
    result->rs_consumer = future_consumer;}
  else if (KNO_TYPEP(consumer,kno_basket_type)) {
    result->rs_output = consumer;
    result->rs_consumer = basket_consumer;}
  else if ( consumer == list_consumer) {
    result->rs_output = KNO_EMPTY_LIST;
    result->rs_consumer = list_consumer;}
  else if ( consumer == choice_consumer) {
    result->rs_output    = kno_init_prechoice(NULL,100,1);
    result->rs_consumer  = choice_consumer;}
  else if ( consumer == basket_consumer) {
    result->rs_output    = kno_init_basket(NULL,100,KNO_EMPTY);
    result->rs_consumer  = basket_consumer;}
  else if (KNO_FALSEP(consumer)) {
    result->rs_output    = KNO_EMPTY;
    result->rs_consumer  = KNO_FALSE;}
  else {
    result->rs_output = KNO_EMPTY_LIST;
    result->rs_consumer = list_consumer;}

  result->rs_framefn  = kno_getopt(opts,KNOSYM(framefn),KNO_FALSE);

  lispval stop_val = kno_getopt(opts,KNOSYM(stop),KNO_VOID);
  long long stop_count = (KNO_VOIDP(stop_val)) ? (-1) :
    (KNO_FIXNUMP(stop_val)) ? (KNO_FIX2INT(stop_val)) : (-1);
  kno_decref(stop_val);

  result->rs_count = 0;
  result->rs_stop = stop_count;
  result->rs_run_count = 0;
  u8_init_condvar(&(result->rs_stopvar));
  u8_init_mutex(&(result->rs_stoplock));

  lispval foldcase = (kno_getopt(opts,KNOSYM(foldcase),KNO_TRUE));
  if (KNO_TRUEP(foldcase))
    result->rs_bits |= KNO_READSTAT_FOLDCASE;
  else NO_ELSE;
  kno_decref(foldcase);

  lispval sortmap = (kno_getopt(opts,KNOSYM(sortmap),KNO_TRUE));
  if (KNO_TRUEP(sortmap))
    result->rs_bits |= KNO_READSTAT_SORTMAP;
  else NO_ELSE;
  kno_decref(sortmap);

  long long use_start = kno_int_getopt(opts,KNOSYM(start),-1);
  result->rs_row_offset = use_start;
  long long use_limit = kno_int_getopt(opts,KNOSYM(limit),-1);
  result->rs_row_limit = use_limit;

  long long max_errs = kno_int_getopt(opts,KNOSYM(maxerrs),0);
  result->rs_max_errs = max_errs;
  result->rs_err_count = 0;

  return result;
 err_exit:
  readstat_recycler(result);
  return NULL;
}

static readstat_parser_t *readstat_start_parser(kno_readstat rs,int just_metadata)
{
  readstat_parser_t *parser = readstat_parser_init();
  if (rs->rs_parser) {
    readstat_parser_free(rs->rs_parser);
    rs->rs_parser=NULL;}
  rs->rs_parser = parser;

  readstat_error_t err;

  time_t mtime = u8_file_mtime(rs->rs_source);
  if (1) { /* ( (rs->rs_filetime<0) || (mtime != rs->rs_filetime) ) */
    err = readstat_set_metadata_handler(parser,metadata_handler);
    CHECK_READSTAT_ERR(err,"readstat_open/set_metadata_handler",rs,NULL);
    err = readstat_set_variable_handler(parser,variable_handler);
    CHECK_READSTAT_ERR(err,"readstat_open/set_variable_handler",rs,NULL);
    err = readstat_set_value_label_handler(parser,label_handler);
    CHECK_READSTAT_ERR(err,"readstat_open/set_label_handler",rs,NULL);}
  rs->rs_filetime=mtime;

  if (! just_metadata) {
    if ( rs->rs_fullread >= 0 ) rs->rs_fullread = 1;
    err = readstat_set_value_handler(parser,value_handler);
    CHECK_READSTAT_ERR(err,"readstat_open/set_value_handler",rs,NULL);
    if (rs->rs_row_limit>=0) {
      err = readstat_set_row_limit(parser,rs->rs_row_limit);
      CHECK_READSTAT_ERR(err,"readstat_open/set_row_limit",rs,NULL);}
    if (rs->rs_row_offset>=0) {
      err = readstat_set_row_offset(parser,rs->rs_row_offset);
      CHECK_READSTAT_ERR(err,"readstat_open/set_row_offset",rs,NULL);}
    rs->rs_running = 1;}
  else {
    if ( rs->rs_fullread >= 0 ) rs->rs_fullread = 0;
    err = readstat_set_progress_handler(parser,progress_handler);
    /* These doesn't seem to keep it from scanning all the pages */
#if 0
    err = readstat_set_row_limit(parser,0);
    CHECK_READSTAT_ERR(err,"readstat_open/set_row_limit/zero",rs,NULL);
    err = readstat_set_row_offset(parser,0);
    CHECK_READSTAT_ERR(err,"readstat_open/set_row_offset/zero",rs,NULL);
    err = readstat_set_row_limit(parser,0);
    CHECK_READSTAT_ERR(err,"readstat_open/set_row_limit/zero",rs,NULL);
#endif
  }
  return parser;
}

static kno_readstat readstat_open(kno_readstat rs,int just_metadata)
{
  readstat_parser_t *parser = readstat_start_parser(rs,just_metadata);
  /* Read metadata or metadata+data */
  int err = rs->rs_parsefn(parser,rs->rs_source,rs);
  if (err != READSTAT_ERROR_USER_ABORT) {
    CHECK_READSTAT_ERR(err,"readstat_open/parse",rs,NULL);}
  else NO_ELSE;

  if (just_metadata) {
    finalize_readstat_template(rs);
    readstat_parser_free(parser);
    parser = rs->rs_parser = NULL;
    rs->rs_running=0;}
  else {
    /* Finish the final observation */
    int rv=0;
    if (rs->rs_obsid>=0) rv=finish_observation(rs);
    consume_observation(rs,KNO_EOD,-1);
    rs->rs_running=0;
    rs->rs_finished=1;}

  return rs;
}

static readstat_error_t readstat_scan
(kno_readstat rs,long long limit,long long start)
{
  if ( (rs->rs_parser) || (rs->rs_running) ) {
    u8_log(LOGWARN,"ReadstatRunning","Readstat %s already running",rs->rs_source);
    return 0;}
  else if (rs->rs_finished) {
    u8_log(LOGWARN,"ReadstatFinished","Readstat %s finished running",rs->rs_source);
    return 0;}
  if (limit>=0) rs->rs_row_limit = limit;
  if (start>=0) rs->rs_row_offset = start;
  readstat_parser_t *parser = readstat_start_parser(rs,0);
  /* Read metadata or metadata+data */
  int rv = rs->rs_parsefn(parser,rs->rs_source,rs);
  if ( (rv == READSTAT_OK) || READSTAT_ERROR_USER_ABORT ) {
    /* Finish the final observation */
    int rv=0;
    if (rs->rs_obsid>=0) rv=finish_observation(rs);
    consume_observation(rs,KNO_EOD,-1);
    rs->rs_running=0;
    rs->rs_run_count++;
    /* This should allow a restart */
    if (rs->rs_parser) {
      readstat_parser_free(rs->rs_parser);
      rs->rs_parser=NULL;}
    return 1;}
  else {
    grab_readstat_err(rv,"readstat_scan/parse",(lispval)rs);
    return -1;}
}

static void reset_metadata(struct KNO_READSTAT *rs)
{
  reset_string(&(rs->rs_table_name));
  reset_string(&(rs->rs_file_label));
  reset_string(&(rs->rs_enc_name));
}

static void readstat_recycler(struct KNO_READSTAT *rs)
{
  int n_slots = rs->rs_n_slots;
  readstat_parser_free(rs->rs_parser); rs->rs_parser=NULL;
  reset_string(&(rs->rs_source));
  reset_string(&(rs->rs_table_name));
  reset_string(&(rs->rs_file_label));
  reset_string(&(rs->rs_enc_name));
  reset_string(&(rs->rs_type));
  kno_decref(rs->annotations);
  kno_decref(rs->rs_colinfo);
  kno_decref(rs->rs_fields);
  if (rs->rs_template) {
    lispval v = (lispval) rs->rs_template;
    kno_decref(v);}
  kno_decref(rs->rs_vlabels);
  if (rs->rs_observation) {
    kno_decref(((lispval)rs->rs_observation));}
  kno_decref(rs->rs_options);
  kno_decref(rs->rs_consumer);
  kno_decref(rs->rs_output);
  if (rs->rs_template_maps) {
    kno_decref_elts(rs->rs_template_maps,n_slots);
    u8_free(rs->rs_template_maps);}
  if (rs->rs_sortmap) u8_free(rs->rs_sortmap);
  if (rs->rs_inv_sortmap) u8_free(rs->rs_inv_sortmap);
  u8_destroy_mutex(&(rs->rs_stoplock));
  u8_destroy_condvar(&(rs->rs_stopvar));
  if (!(KNO_CONS_STATICP(rs))) u8_free(rs);
}

static void recycle_readstat(struct KNO_RAW_CONS *c)
{
  struct KNO_READSTAT *rs = (struct KNO_READSTAT *)c;
  readstat_recycler(rs);
}

static int unparse_readstat(struct U8_OUTPUT *out,lispval x,
                            kno_printopts popts,int depth)
{
  struct KNO_READSTAT *rs = (struct KNO_READSTAT *)x;
  u8_string name = rs->rs_table_name;
  if ( (name==NULL) || (*name=='\0') )
    name=rs->rs_file_label;
  else NO_ELSE;
  u8_string err_status="";
  u8_exception ex = rs->rs_exception;
  U8_FIXED_OUTPUT(err,100);
  if (ex) {
    u8_printf(errout,"err=%m<%s>",ex->u8x_cond,ex->u8x_context);
    if (ex->u8x_details)
      u8_printf(errout,"<%s>",ex->u8x_details);
    err_status=u8_outstring(errout);}
  if (rs->rs_n_rows!=rs->rs_count) {
    if (name)
      u8_printf(out,"#<READSTAT/%s '%s'%s %d cols %lld/%lld rows '%s' #!%llx>",
                rs->rs_type,name,err_status,rs->rs_n_vars,
                rs->rs_n_rows,rs->rs_count,
                rs->rs_source,rs);
    else u8_printf(out,"#<READSTAT/%s%s %d cols %lld/%lld rows '%s' #!%llx>",
                   rs->rs_type,err_status,
                   rs->rs_n_vars,rs->rs_count,rs->rs_n_rows,
                   rs->rs_source,rs);}
  else if (name)
    u8_printf(out,"#<READSTAT/%s '%s'%s %d cols %lld rows '%s' #!%llx>",
              rs->rs_type,name,err_status,
              rs->rs_n_vars,rs->rs_n_rows,rs->rs_source,rs);
  else u8_printf(out,"#<READSTAT/%s%s %d cols %lld rows '%s' #!%llx>",
                 rs->rs_type,err_status,
                 rs->rs_n_vars,rs->rs_n_rows,rs->rs_source,rs);
  return 1;
}

/* Open functions */

DEFC_PRIM("creadstat/open",creadstat_open,
          KNO_MAX_ARGS(5)|KNO_MIN_ARGS(1),
          "Opens a readstat parser for *file*",
          {"file",kno_string_type,KNO_VOID},
          {"opts",kno_opts_type,KNO_VOID},
          {"type",kno_any_type,KNO_VOID},
          {"consumer",kno_any_type,KNO_VOID},
          {"output",kno_any_type,KNO_VOID})
static lispval creadstat_open(lispval filename,lispval opts,lispval type,
                              lispval consumer,lispval output)
{
  u8_string source = KNO_CSTRING(filename);
  int free_consumer=0, free_output=0;
  u8_string typename = NULL;
  if (KNO_SYMBOLP(type))
    typename = KNO_PNAME(type);
  else if (KNO_STRINGP(type))
    typename = KNO_CSTRING(type);
  else if ( (KNO_VOIDP(type)) || (KNO_FALSEP(type)) || (KNO_DEFAULTP(type)) ) {
    if (!(kno_testopt(opts,KNOSYM(filetype),KNO_VOID))) {
      u8_string suffix = strrchr(source,'.');
      if (suffix) typename = suffix+1;}
    else NO_ELSE;}
  else return kno_err(_C("MissingFileType"),"readstat_open_prim",source,type);
  kno_readstat result = new_readstat(source,typename,opts,consumer,output);
  if (result == NULL) return KNO_ERROR;
  lispval init = kno_getopt(opts,KNOSYM(init),KNO_TRUE);
  if ( init == KNO_FALSE )
    return (lispval) result;
  else kno_decref(init);
  kno_readstat opened = readstat_open(result,1);
  if (opened)
    return (lispval) opened;
  else return KNO_ERROR;
}

DEFC_PRIM("creadstat/scan",creadstat_scan,
          KNO_MAX_ARGS(2)|KNO_MIN_ARGS(1),
          "Begins scanning data from the readstat object *creadstat*",
          {"creadstat",KNO_READSTAT_TYPE,KNO_VOID},
          {"opts",kno_opts_type,KNO_FALSE})
static lispval creadstat_scan(lispval creadstat,lispval opts)
{
  kno_readstat rs = (kno_readstat) creadstat;
  long long start = kno_int_getopt(opts,KNOSYM(start),-1);
  long long limit = kno_int_getopt(opts,KNOSYM(limit),-1);
  int start_count = rs->rs_count;
 int rv = readstat_scan(rs,limit,start);
  if (rv<0)
    return KNO_ERROR;
  else {
    int added = rs->rs_count-start_count;
    return KNO_INT(added);}
}

/* Readstat properties */

DEFC_PRIM("readstat?",readstatp,
	  KNO_MAX_ARGS(1)|KNO_MIN_ARGS(1),
          "Returns true if *arg* is a readstat object",
          {"arg",kno_any_type,KNO_VOID})
static lispval readstatp(lispval arg)
{
  if (KNO_TYPEP(arg,kno_readstat_type))
    return KNO_TRUE;
  else return KNO_FALSE;
}

DEFC_PRIM("readstat-source",readstat_source,
	  KNO_MAX_ARGS(1)|KNO_MIN_ARGS(1),
	  "Gets the source of a readstat object",
	  {"arg",KNO_READSTAT_TYPE,KNO_VOID})
static lispval readstat_source(lispval arg)
{
  kno_readstat rs = (kno_readstat) arg;
  if (rs->rs_source)
    return makestring(rs->rs_source);
  else return KNO_FALSE;
}

DEFC_PRIM("readstat-type",readstat_type,
	  KNO_MAX_ARGS(1)|KNO_MIN_ARGS(1),
	  "Gets the file type of a readstat object",
	  {"arg",KNO_READSTAT_TYPE,KNO_VOID})
static lispval readstat_type(lispval arg)
{
  kno_readstat rs = (kno_readstat) arg;
  if (rs->rs_type)
    return kno_intern(rs->rs_type);
  else return KNO_FALSE;
}

DEFC_PRIM("readstat-template",readstat_template,
	  KNO_MAX_ARGS(1)|KNO_MIN_ARGS(1),
	  "Gets the dataframe template for a readstat object",
	  {"arg",KNO_READSTAT_TYPE,KNO_VOID})
static lispval readstat_template(lispval arg)
{
  kno_readstat rs = (kno_readstat) arg;
  if (rs->rs_template) {
    lispval df = (lispval) (rs->rs_template);
    return kno_incref(df);}
  else return KNO_FALSE;
}

DEFC_PRIM("readstat-options",readstat_options,
	  KNO_MAX_ARGS(1)|KNO_MIN_ARGS(1),
	  "Gets the options with which a readstat object was created",
	  {"arg",KNO_READSTAT_TYPE,KNO_VOID})
static lispval readstat_options(lispval arg)
{
  kno_readstat rs = (kno_readstat) arg;
  if (rs->rs_options) {
    lispval df = (lispval) (rs->rs_options);
    return kno_incref(df);}
  else return KNO_FALSE;
}

DEFC_PRIM("readstat-fields",readstat_fields,
	  KNO_MAX_ARGS(1)|KNO_MIN_ARGS(1),
	  "Gets the column info of a readstat object",
	  {"arg",KNO_READSTAT_TYPE,KNO_VOID})
static lispval readstat_fields(lispval arg)
{
  kno_readstat rs = (kno_readstat) arg;
  if (rs->rs_colinfo) {
    lispval df = rs->rs_colinfo;
    return kno_incref(df);}
  else return KNO_FALSE;
}

DEFC_PRIM("readstat-rows",readstat_rows,
	  KNO_MAX_ARGS(1)|KNO_MIN_ARGS(1),
	  "Returns the number of rows in the dataset",
	  {"arg",KNO_READSTAT_TYPE,KNO_VOID})
static lispval readstat_rows(lispval arg)
{
  kno_readstat rs = (kno_readstat) arg;
  return KNO_INT(rs->rs_n_rows);
}

DEFC_PRIM("readstat-columns",readstat_columns,
	  KNO_MAX_ARGS(1)|KNO_MIN_ARGS(1),
	  "Returns the number of columns in the dataset",
	  {"arg",KNO_READSTAT_TYPE,KNO_VOID})
static lispval readstat_columns(lispval arg)
{
  kno_readstat rs = (kno_readstat) arg;
  return KNO_INT(rs->rs_n_vars);
}

DEFC_PRIM("readstat-labels",readstat_labels,
	  KNO_MAX_ARGS(1)|KNO_MIN_ARGS(1),
	  "Gets the labels of the readstat object",
	  {"arg",KNO_READSTAT_TYPE,KNO_VOID})
static lispval readstat_labels(lispval arg)
{
  kno_readstat rs = (kno_readstat) arg;
  return kno_incref(rs->rs_vlabels);
}

DEFC_PRIM("readstat-consumer",readstat_consumer,
	  KNO_MAX_ARGS(1)|KNO_MIN_ARGS(1),
	  "Gets the consumer of the readstat object",
	  {"arg",KNO_READSTAT_TYPE,KNO_VOID})
static lispval readstat_consumer(lispval arg)
{
  kno_readstat rs = (kno_readstat) arg;
  return kno_incref(rs->rs_consumer);
}

DEFC_PRIM("readstat-output",readstat_output,
	  KNO_MAX_ARGS(1)|KNO_MIN_ARGS(1),
	  "Gets the output of the readstat object, if available",
	  {"arg",KNO_READSTAT_TYPE,KNO_VOID})
static lispval readstat_output(lispval arg)
{
  kno_readstat rs = (kno_readstat) arg;
  if (KNO_VOIDP(rs->rs_output))
    return KNO_QVOID;
  else return kno_make_simple_choice(rs->rs_output);
}

DEFC_PRIM("readstat-count",readstat_count,
	  KNO_MAX_ARGS(1)|KNO_MIN_ARGS(1),
	  "Gets the number of records processed by the readstat object",
	  {"arg",KNO_READSTAT_TYPE,KNO_VOID})
static lispval readstat_count(lispval arg)
{
  kno_readstat rs = (kno_readstat) arg;
  return KNO_INT(rs->rs_count);
}

DEFC_PRIM("readstat-stop",readstat_stop,
	  KNO_MAX_ARGS(1)|KNO_MIN_ARGS(1),
	  "Gets the maximum number of records to read before pausing",
	  {"arg",KNO_READSTAT_TYPE,KNO_VOID})
static lispval readstat_stop(lispval arg)
{
  kno_readstat rs = (kno_readstat) arg;
  if (rs->rs_stop<0)
    return KNO_FALSE;
  else return KNO_INT(rs->rs_stop);
}

DEFC_PRIM("readstat-advance",readstat_advance,
          KNO_MAX_ARGS(2)|KNO_MIN_ARGS(2),
          "Gets the number of records processed by the readstat object",
          {"arg",KNO_READSTAT_TYPE,KNO_VOID},
          {"limit",kno_fixnum_type,KNO_VOID})
static lispval readstat_advance(lispval arg,lispval limit)
{
  kno_readstat rs = (kno_readstat) arg;
  long long lim = KNO_FIX2INT(limit);
  long long curval = rs->rs_stop;
  if (lim>curval) {
    u8_lock_mutex(&(rs->rs_stoplock));
    long long cur = rs->rs_stop;
    if (lim > cur) rs->rs_stop=lim;
    u8_condvar_broadcast(&(rs->rs_stopvar));
    u8_unlock_mutex(&(rs->rs_stoplock));
    return KNO_INT(curval);}
  else return KNO_FALSE;
}

DEFC_PRIM("readstat-finished?",readstat_finishedp,
	  KNO_MAX_ARGS(1)|KNO_MIN_ARGS(1),
	  "Returns true if the readstat object is finished processing data",
	  {"arg",KNO_READSTAT_TYPE,KNO_VOID})
static lispval readstat_finishedp(lispval arg)
{
  kno_readstat rs = (kno_readstat) arg;
  if (rs->rs_finished)
    return KNO_TRUE;
  else return KNO_FALSE;
}

DEFC_PRIM("readstat-run-count",readstat_run_count,
	  KNO_MAX_ARGS(1)|KNO_MIN_ARGS(1),
	  "Returns true if the readstat object is finished processing data",
	  {"arg",KNO_READSTAT_TYPE,KNO_VOID})
static lispval readstat_run_count(lispval arg)
{
  kno_readstat rs = (kno_readstat) arg;
  return KNO_INT(rs->rs_run_count);
}

DEFC_PRIM("readstat-running?",readstat_runningp,
	  KNO_MAX_ARGS(1)|KNO_MIN_ARGS(1),
	  "Returns true if the readstat object is actively processing data",
	  {"arg",KNO_READSTAT_TYPE,KNO_VOID})
static lispval readstat_runningp(lispval arg)
{
  kno_readstat rs = (kno_readstat) arg;
  if (rs->rs_running)
    return KNO_TRUE;
  else return KNO_FALSE;
}

/* Creating a readstat object */

KNO_EXPORT int kno_initialize_creadstat()
{
  if (readstat_initialized) return 0;
  readstat_initialized = 1;
  KNO_INITIALIZE();

  kno_readstat_type = kno_register_cons_type("readstat_db",KNO_READSTAT_TYPE);
  kno_unparsers[kno_readstat_type] = unparse_readstat;
  kno_recyclers[kno_readstat_type] = recycle_readstat;
  kno_tablefns[kno_readstat_type] = kno_annotated_tablefns;

#ifdef KNO_MISSING_VALUE
  system_missing_value = KNO_MISSING_VALUE;
#else
  system_missing_value = kno_register_constant("#missing_value");
#endif
  /* Add alternative missing value types for some file formats */
  char *tagged_missing_template="#missing_value_?";
  size_t template_tail=strlen(tagged_missing_template)-1;
  int i = 0; while (i< 26) {
    u8_byte *const_name=u8_strdup(tagged_missing_template);
    const_name[template_tail]='a'+i;
    tagged_missing_values[i]=kno_register_constant(const_name);
    u8_free(const_name);
    i++;}

  list_consumer = kno_intern("list");
  choice_consumer = kno_intern("choice");
  future_consumer = kno_intern("future");
  basket_consumer = kno_intern("basket");
  countvec_consumer = kno_intern("countvec");
  templateid_symbol = kno_intern("templateid");

  kno_add_readstat_parser("sas7bdat",readstat_parse_sas7bdat);
  kno_add_readstat_parser("sas7bcat",readstat_parse_sas7bcat);
  kno_add_readstat_parser("dta",readstat_parse_dta);
  kno_add_readstat_parser("sav",readstat_parse_sav);
  kno_add_readstat_parser("por",readstat_parse_por);
  kno_add_readstat_parser("xport",readstat_parse_xport);

  creadstat_module = kno_new_cmodule
    ("creadstat",0,kno_initialize_creadstat);

  kno_store(creadstat_module,kno_intern("readstat-lisp-type"),
            KNO_CTYPE(kno_readstat_type));
  lispval sourceinfo = makestring(_FILEINFO);
  kno_add(creadstat_module,kno_intern("%fileinfo"),sourceinfo);
  kno_decref(sourceinfo);

  link_local_cprims();
  kno_finish_module(creadstat_module);

  u8_register_source_file(_FILEINFO);

  return 1;
}

static void link_local_cprims()
{
  KNO_LINK_CPRIM("creadstat/open",creadstat_open,5,creadstat_module);
  KNO_LINK_CPRIM("creadstat/scan",creadstat_scan,2,creadstat_module);

  KNO_LINK_CPRIM("readstat?",readstatp,1,creadstat_module);

  KNO_LINK_CPRIM("readstat-source",readstat_source,1,creadstat_module);
  KNO_LINK_CPRIM("readstat-template",readstat_template,1,creadstat_module);
  KNO_LINK_CPRIM("readstat-options",readstat_options,1,creadstat_module);
  KNO_LINK_CPRIM("readstat-fields",readstat_fields,1,creadstat_module);
  KNO_LINK_CPRIM("readstat-type",readstat_source,1,creadstat_module);
  KNO_LINK_CPRIM("readstat-labels",readstat_labels,1,creadstat_module);
  KNO_LINK_CPRIM("readstat-consumer",readstat_consumer,1,creadstat_module);
  KNO_LINK_CPRIM("readstat-output",readstat_output,1,creadstat_module);
  KNO_LINK_CPRIM("readstat-count",readstat_count,1,creadstat_module);
  KNO_LINK_CPRIM("readstat-rows",readstat_rows,1,creadstat_module);
  KNO_LINK_CPRIM("readstat-columns",readstat_columns,1,creadstat_module);
  KNO_LINK_CPRIM("readstat-stop",readstat_stop,1,creadstat_module);
  KNO_LINK_CPRIM("readstat-advance",readstat_advance,2,creadstat_module);
  KNO_LINK_CPRIM("readstat-run-count",readstat_run_count,1,creadstat_module);
  KNO_LINK_CPRIM("readstat-finished?",readstat_finishedp,1,creadstat_module);
  KNO_LINK_CPRIM("readstat-running?",readstat_runningp,1,creadstat_module);

  KNO_LINK_ALIAS("readstat-varcount",readstat_columns,creadstat_module);

}

