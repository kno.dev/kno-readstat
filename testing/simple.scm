;;; -*- Mode: Scheme; -*-

(use-module 'readstat)

(use-module 'fifo)

(define sample1 (get-component "samples/grfc_tab20_01001.sas7bdat"))
(define sample2 (get-component "samples/grfn_tab20_01.sas7bdat"))
(define sample3 (get-component "samples/WGI.dta"))

(define (readstat/test sample . args)
  (let* ((r (apply readstat/open sample args))
         (init-output (readstat-output r)))
    (cond ((and (exists? init-output) (fifo? init-output))
           (let ((read-thread (thread/call readstat/scan r))
                 (use-thread (thread/call drain-fifo r)))
             (thread/join read-thread)
             init-output))
          (else (readstat/scan r)))
    (readstat/scan r)
    (let ((output (readstat-output r)))
      (%watch "TEST"
        r "count" (readstat-count r)
        "output" (typeof (readstat-output r))
        "size" (cond ((ambiguous? output) (choice-size output))
                     ((pair? output) (length output))
                     ((basket? output) (choice-size output))
                     (else (printout " unknown for " output)))))
    r))

(define (drain-fifo fifo)
  (let ((count 0)
        (item (fifo/pop fifo)))
    (while (fifo-live? fifo)
      (set! count (1+ count))
      (set! item (fifo/pop fifo)))
    count))

(define (test-sample sample)
  (readstat/test sample)
  (readstat/test sample #[idslot geoid])
  (readstat/test sample #[idslot geoid] 'choice)
  (readstat/test sample 'list)
  (readstat/test sample 'choice)
  (readstat/test sample 'countvec)
  (readstat/test sample 'basket)
  (let ((counter 0))
    (let ((readstat (readstat/test sample #f (lambda (frame id) (sset! counter (1+ counter))))))
      (%watch "READSTAT-CALLBACK" counter (readstat-count readstat) readstat))))

;; (define (test-sample sample)
;;   (readstat/test sample))

(define (main)
  (test-sample sample1)
  (test-sample sample2)
  (test-sample sample3))

