;;; -*- Mode: Scheme; -*-

(use-module 'readstat)

(define sample1 (get-component "samples/grfc_tab20_01001.sas7bdat"))
(define sample2 (get-component "samples/grfn_tab20_01.sas7bdat"))
(define sample3 (get-component "samples/WGI.dta"))

(define (readstat/test sample . args)
  (let ((r (apply readstat/open sample args)))
    (readstat/scan r)
    (let ((output (readstat-output r)))
      (%watch "TEST"
        r "count" (readstat-count r)
        "output" (typeof (readstat-output r))
        "size" (cond ((ambiguous? output) (choice-size output))
                     ((pair? output) (length output))
                     ((basket? output) (choice-size output))
                     (else (printout " unknown for " output)))))))

(define (test-sample sample)
  (readstat/test sample)
  (readstat/test sample 'list)
  (readstat/test sample 'choice)
  (readstat/test sample 'countvec)
  (readstat/test sample 'basket))

(define (main)
  (readstat/test sample1))


