;;; -*- Mode: Scheme; Character-encoding: utf-8; -*-
;;; Copyright (C) 2005-2021 beingmeta, inc.  All rights reserved.
;;; Copyright (C) 2021-2024 Kenneth Haase.  All rights reserved.

(in-module 'readstat)

;; This is the C wrapper for the readstat library
(use-module 'creadstat)

(use-module '{texttools fifo})

(module-export! '{readstat/open readstat/scan readstat/read})

(define %loglevel %warn%)

(define creadstat (get-module 'creadstat))

(define (readstat/open file (opts #f) (consumer) . args)
  "Open and scan the data file *file* based on *opts* and "
  "handling the outputs with *consumer* and *args*. "
  (default! consumer (getopt opts 'consumer (getopt opts 'output 'choice)))
  (local output (getopt opts 'output #default))
  (unless (file-exists? file)
    ;; Maybe do something special with non file sources (get and cache)
    (irritant file |NoSuchFile|))
  (when (or (overlaps? opts '{list choice basket countvec})
            (applicable? opts) (basket? opts) (future? opts) (fifo? opts)
            (and (pair? opts) (applicable? (car opts))))
    ;; Support a convenience where the consumer comes before the opts
    (let ((use-opts (and (opts? consumer) consumer)))
      (set! consumer opts)
      (set! opts use-opts)))
  (cond ((symbol? consumer))
        ((applicable? consumer)
         (if (pair? args)
             (set! consumer (cons consumer (->vector args)))))
        ;; These are all handled intrinsically
        ((basket? consumer))
        ((future? consumer))
        ((and (pair? consumer) (applicable? (car consumer))))
        ((fifo? consumer)
         (set! output consumer)
         (set! consumer fifo-consumer))
        ((or (basket? output) (future? output))
         (set! consumer output)
         (set! output #default))
        ((fifo? output)
         (set! consumer fifo-consumer))
        (else (irritant consumer |InvalidOutput|)))
  (lognotice |Readstat/Open|
    "Opening " file " to feed "
    (if (applicable? consumer) output consumer)
    "\n opts: " ($pprint opts))
  (let ((rs (creadstat/open file opts #default consumer output)))
    (store! rs 'opened (elapsed-time))
    rs))

(define (fifo-consumer frame id readstat output)
  (if (or (eod? frame) (< id 0))
      ;; This tells the FIFO we're done adding things, so
      ;;  it will be exhaused when the items are all done
      (let* ((opened (get readstat 'opened))
             (options (readstat-options readstat))
             (maxitems (getopt options 'maxitems))
             (oneshot (getopt options 'oneshot)))
        (lognotice |ReadstatEOD|
          "After " (when (and (exists? opened) opened)
                     (printout (secs->string (elapsed-time opened)) " and "))
          (readstat-count readstat) " records, v=" frame
          " from " readstat " writing to " output)
        (when (or oneshot (and maxitems (> (readstat-count readstat) maxitems)))
            (fifo/readonly! output)))
      (fifo/push! output frame)))

(define (readstat/read file (opts #f) (filetype) (consumer) (output))
  "Opens a readstat object for *file* and scans all of its data, "
  "processing rows using *consumer* and (optionall) *output*. "
  "See readstat/open for those arguments. Alternately, if *file* is "
  "a readstat object, it's data is scanned."
  (local args
    (cond ((bound? output) (list filetype consumer output))
          ((bound? consumer) (list filetype consumer))
          ((bound? filetype) (list filetype))
          (else '())))
  (if (string? file)
      (creadstat/scan (if (empty-list? args)
                          (readstat/open file opts)
                          (apply readstat/open file opts args))
                      opts)
      (creadstat/scan file opts)))

(define readstat/scan (fcn/alias readstat/read))

(define readstat-consumer (get creadstat 'readstat-consumer))
(define readstat-options (get creadstat 'readstat-options))
(define readstat-output (get creadstat 'readstat-output))
(define readstat-labels (get creadstat 'readstat-labels))
(define readstat-template (get creadstat 'readstat-template))
(define readstat-fields (get creadstat 'readstat-fields))
(define readstat-source (get creadstat 'readstat-source))
(define readstat-type (get creadstat 'readstat-type))
(define readstat-count (get creadstat 'readstat-count))
(define readstat-finished? (get creadstat 'readstat-finished?))
(define readstat-running? (get creadstat 'readstat-running?))

(module-export! '{readstat-source readstat-type
		  readstat-template readstat-colinfo readstat-labels
		  readstat-consumer readstat-output
		  readstat-count readstate-limit
                  readstat-start readstat-advance
                  readstat-finished? readstat-running?})
